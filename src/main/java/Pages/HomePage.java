package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    @FindBy(xpath = "//input[@id='input_search']")
    private WebElement searchInput;

    @FindBy(xpath = "//button[@class='button-reset search-btn']")
    private WebElement searchButton;

    @FindBy(xpath = "//span[@class='sidebar-item']")
    private WebElement productCatalog;

    @FindBy(xpath = "//ul[contains(@class,'sidebar-list')]//a[contains(@href, 'apple-store')]")
    private WebElement appleStoreOptionInProductCatalog;

    @FindBy(xpath = "//li[contains(@class,'parent js_sidebar-item')]/a[contains(@href,'telefonyi-i-aksessuaryi')]")
    private WebElement SmartPhonesAndPhonesOptionInProductCatalog;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void searchByKeyword(final String keyword){
        searchInput.sendKeys(keyword);
        searchButton.click();
    }

    public void openAppleStorePage(){
        productCatalog.click();
        appleStoreOptionInProductCatalog.click();
    }

    public void openSmartPhonesAndPhonesPage(){
        SmartPhonesAndPhonesOptionInProductCatalog.click();
    }
}
