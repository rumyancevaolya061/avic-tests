package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SmartphonesAndPhonesPage extends BasePage{

    @FindBy(xpath = "//div[contains(@class,'brand-box__title')]//a[contains(@href,'avic.ua/ua/smartfonyi')]")
    private WebElement smartphones;

    public SmartphonesAndPhonesPage(WebDriver driver) {
        super(driver);
    }

    public void openSmartphonesPage(){
        smartphones.click();
    }
}
