package Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class ProductsListingPage extends BasePage{

    @FindBy(xpath = "//div[contains(@class,'filter-area')]//a[contains(@href,'samsung')]")
    private WebElement filteringByBrandOption;

    @FindBy(xpath = "//div[contains(@class,'item-prod')]")
    private List<WebElement> filteredByBrandProducts;

    @FindBy(xpath = "//input[contains(@class,'form-control-min')]")
    private WebElement filteringByPriceMinPrice;

    @FindBy(xpath = "//input[contains(@class,'form-control-max')]")
    private WebElement filteringByPriceMaxPrice;

    @FindBy(xpath = "//div[contains(@class,'js_filter_parent open-filter-tooltip')]//span[@class='filter-tooltip-inner']")
    private WebElement applyFilteringByPriceButton;

    @FindBy(xpath = "//div[@class='prod-cart__prise']")
    private List<WebElement> filteredByPriceProductsList;

    @FindBy(xpath = "//div[@class='prod-cart__addto']")
    private List<WebElement> addToWishlistButtons;

    @FindBy(xpath = "//div[@class='category-top']//span[@class='selection']//span[contains(@class,'select2-selection--single')]")
    private WebElement sortingDropdown;

    @FindBy(xpath = "//li[contains(@id,'priceasc')]")
    private WebElement sortingFromCheapToExpensiveOption;

    @FindBy(xpath = "//div[contains(@class,'prise-new')]")
    private List<WebElement> pricesOfElementsSortedFromCheapToExpensive;

    @FindBy(xpath = "//a[@class='prod-cart__buy']")
    private List<WebElement> addToCartButtons;

    @FindBy(id = "js_cart")
    private WebElement cartPopup;

    @FindBy(xpath = "//div[@class='btns-cart-holder']//a[contains(@class,'btn--orange')]")
    private WebElement continueShopping;

    @FindBy(xpath = "//div[contains(@class,'header-bottom__cart')]//div[contains(@class,'cart_count')]")
    private WebElement currentCartCounter;

    public ProductsListingPage(WebDriver driver) {
        super(driver);
    }

    public void waitForApplyFilteringByPriceButtonAppears(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.visibilityOf(applyFilteringByPriceButton));
    }

    public void filteringByBrand(){
        filteringByBrandOption.click();
    }

    public List<WebElement> getFilteredByBrandProductsList(){
        return filteredByBrandProducts;
    }

    public void filteringByPrice(String minPrice, String maxPrice){
        filteringByPriceMinPrice.sendKeys(Keys.chord(minPrice,Keys.ENTER));
        filteringByPriceMaxPrice.sendKeys(Keys.chord(Keys.CONTROL,Keys.BACK_SPACE));
        filteringByPriceMaxPrice.sendKeys(Keys.chord(maxPrice,Keys.ENTER));
        waitForApplyFilteringByPriceButtonAppears();
        applyFilteringByPriceButton.click();
    }

    public List<WebElement> getFilteredByPriceProductsList(){ return filteredByPriceProductsList; }

    public void addToWishlist(){ addToWishlistButtons.get(2).click(); }

    public void waitForSortingDropdownOpening(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.visibilityOf(sortingFromCheapToExpensiveOption));

    }

    public void sortingFromCheapToExpensive(){
        sortingDropdown.click();
        waitForSortingDropdownOpening();
        sortingFromCheapToExpensiveOption.click();
    }

    public List<WebElement> getPricesOfElementsSortedFromCheapToExpensive(){ return pricesOfElementsSortedFromCheapToExpensive; }

    public void addToCart(){
        addToCartButtons.get(1).click();
    }

    public void waitForCartPopup(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));//wait for the pop-up with the product added to the cart
        wait.until(ExpectedConditions.visibilityOf(cartPopup));
    }

    public void closeCartPopup(){
        continueShopping.click();
    }

    public String getCurrentCartCounter(){
        return currentCartCounter.getText();
    }

}
