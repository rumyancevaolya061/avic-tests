package avic_test;

import Pages.BasePage;
import org.testng.Assert;
import org.testng.annotations.Test;



public class AddToCartTests extends BaseTest {

    private static final String CART_COUNTER = "1";

    @Test
    public void checkAddToCart() {
        getHomePage().openAppleStorePage();
        getAppleStorePage().openIphonePage();
        getAppleStorePage().waitForPageLoading(30);
        getIphonePage().addToCart();
        getIphonePage().waitForCartPopup();
        getIphonePage().closeCartPopup();
        Assert.assertEquals(getIphonePage().getCurrentCartCounter(), CART_COUNTER);
    }
}
