package avic_test;


import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HelpMethods {

    public static boolean priceValidation(String expectedMinPrice, String expectedMaxPrice, String actualPrice){
        String priceForTest = actualPrice.split(" грн")[0];
        if(Integer.parseInt(priceForTest) >= Integer.parseInt(expectedMinPrice) &&
                Integer.parseInt(priceForTest) <= Integer.parseInt(expectedMaxPrice))
            return true; else
             return false;
    }

    public static List<String> priceSortingFromCheapToExpensiveValidation(List<WebElement> prices){
        List<String> expectedSortedPrices = new ArrayList<String>();
        for (WebElement element : prices){
            element.getText().replaceAll(" ","");
            expectedSortedPrices.add(element.getText());
        }
        Collections.sort(expectedSortedPrices);
        return expectedSortedPrices;
    }

}
