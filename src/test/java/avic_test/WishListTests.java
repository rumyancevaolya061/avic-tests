package avic_test;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WishListTests extends BaseTest{

    private static final String URL_OF_LOG_IN_PAGE = "avic.ua/ua/sign-in";

    @Test
    public void checkWishlistIsAvailableForRegisteredUsers(){
        getHomePage().openSmartPhonesAndPhonesPage();
        getSmartphonesAndPhonesPage().openSmartphonesPage();
        getSmartphonesPage().addToWishlist();
        getSmartphonesPage().waitForPageLoading(30);
        assertTrue(getDriver().getCurrentUrl().contains(URL_OF_LOG_IN_PAGE));
    }

}
