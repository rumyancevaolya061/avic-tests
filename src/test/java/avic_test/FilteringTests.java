package avic_test;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class FilteringTests extends BaseTest {
    private static final String EXPECTED_BRAND_NAME = "Samsung";
    private static final String FILTERING_BY_PRICE_MIN_PRICE = "5000";
    private static final String FILTERING_BY_PRICE_MAX_PRICE = "10000";

    @Test
    public void checkFilteringByBrand(){
        getHomePage().openSmartPhonesAndPhonesPage();
        getSmartphonesAndPhonesPage().openSmartphonesPage();
        getSmartphonesPage().filteringByBrand();
        getSmartphonesPage().waitForPageLoading(30);
        for(WebElement webElement : getSmartphonesPage().getFilteredByBrandProductsList())
            assertTrue(webElement.getText().contains(EXPECTED_BRAND_NAME));
    }

    @Test
    public void checkFilteringByPrice(){
        getHomePage().openSmartPhonesAndPhonesPage();
        getSmartphonesAndPhonesPage().openSmartphonesPage();
        getSmartphonesPage().filteringByPrice(FILTERING_BY_PRICE_MIN_PRICE, FILTERING_BY_PRICE_MAX_PRICE);
        getSmartphonesPage().waitForPageLoading(30);
        for (WebElement element : getSmartphonesPage().getFilteredByPriceProductsList()){
            assertTrue(HelpMethods.priceValidation(FILTERING_BY_PRICE_MIN_PRICE, FILTERING_BY_PRICE_MAX_PRICE, element.getText()));
        }


    }
}
