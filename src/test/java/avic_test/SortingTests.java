package avic_test;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class SortingTests extends BaseTest{

    private List<String> expectedPriceOFElementSortedFromCheapToExpensive;
    @Test
    public void checkSortingFromCheapToExpensive(){
        getHomePage().openSmartPhonesAndPhonesPage();
        getSmartphonesAndPhonesPage().openSmartphonesPage();
        getSmartphonesPage().waitForPageLoading(30);
        getSmartphonesPage().sortingFromCheapToExpensive();
        getSmartphonesPage().waitForPageLoading(30);
        expectedPriceOFElementSortedFromCheapToExpensive = HelpMethods.priceSortingFromCheapToExpensiveValidation(getSmartphonesPage().getPricesOfElementsSortedFromCheapToExpensive());
        for(int i = 0; i < getSmartphonesPage().getPricesOfElementsSortedFromCheapToExpensive().size(); i++){
            assertEquals(getSmartphonesPage().getPricesOfElementsSortedFromCheapToExpensive().indexOf(i),
                    expectedPriceOFElementSortedFromCheapToExpensive.indexOf(i));
        }

    }
}
